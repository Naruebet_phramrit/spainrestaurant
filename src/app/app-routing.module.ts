import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { RestaurantComponent } from './restaurant/restaurant.component'
import { RestaurantListComponent } from './restaurant-list/restaurant-list.component'
import { ReviewComponent } from './review/review.component'

const routes: Routes = [
  { path: '', redirectTo: 'restaurant', pathMatch: 'full' },
  { path: 'restaurant', component: RestaurantListComponent },
  {
    path: 'restaurant/:restaurantId',
    children: [
      { path: '', component: RestaurantComponent },
      { path: 'review/:reviewId', component: ReviewComponent }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
