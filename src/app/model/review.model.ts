export class ReviewModel {
  author: string;
  reviewText: string;
  id: string;
}