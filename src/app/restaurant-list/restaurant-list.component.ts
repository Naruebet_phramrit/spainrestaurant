import { Component, OnInit } from '@angular/core';
import { RestaurantService } from '../services/restaurant.service';
import { Router, ActivatedRoute } from '@angular/router';
import { RestaurantModel } from '../model/restaurant.model';

@Component({
  selector: 'app-restaurant-list',
  templateUrl: './restaurant-list.component.html',
  styleUrls: ['./restaurant-list.component.css']
})
export class RestaurantListComponent implements OnInit {
  restaurantList: RestaurantModel[];
  restaurantId: string[] = [];
  constructor(private restaurantService: RestaurantService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    let counter = 0;
    this.restaurantService.getRestaurantList().subscribe(data => {
      this.restaurantList = data.map(e => {
        this.restaurantId[counter++] = e.payload.doc.id;
        return e.payload.doc.data() as RestaurantModel;
      });
      this.restaurantList = this.restaurantService.mapRestaurantDataWithId(this.restaurantList, this.restaurantId);
      console.log(this.restaurantList);
    });
  }

  ngDoCheck() {
    console.log('ngDocheck');
    console.log(this.restaurantList);
    console.log(this.restaurantId);
  }
  onRestaurantSelect(restaurantId: number) {
    this.router.navigate([restaurantId], {relativeTo: this.activatedRoute});
  }

}
