import { Component, OnInit } from '@angular/core';
import { RestaurantService } from '../services/restaurant.service';
import { ActivatedRoute } from '@angular/router';
import { RestaurantModel } from '../model/restaurant.model';

@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant.component.html',
  styleUrls: ['./restaurant.component.css']
})
export class RestaurantComponent implements OnInit {
  restaurant: RestaurantModel;
  restaurantId: string;
  constructor(private restaurantService: RestaurantService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    const id = this.activatedRoute.snapshot.paramMap.get('restaurantId');
    this.restaurantId = id;
    this.restaurantService.setCurrentRestaurant(this.restaurantId);
    this.restaurantService.getRestaurantInfo(this.restaurantId).subscribe(data => {
      console.log(data);
      this.restaurant = data as RestaurantModel;
    });
  }

}
