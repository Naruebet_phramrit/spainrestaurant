import { Component, OnInit } from '@angular/core';
import { ReviewService } from '../services/review.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ReviewModel } from '../model/review.model';
import { RestaurantService } from '../services/restaurant.service';

@Component({
  selector: 'app-review-list',
  templateUrl: './review-list.component.html',
  styleUrls: ['./review-list.component.css']
})
export class ReviewListComponent implements OnInit {
  reviewList: ReviewModel[] = [];
  reviewId: string[] = [];
  restaurantId: string;
  constructor(
    private restaurantService: RestaurantService,
    private reviewService: ReviewService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    let counter = 0;
    const restaurantId = this.activatedRoute.snapshot.paramMap.get('restaurantId');
    this.restaurantId = restaurantId;
    this.reviewService.getReviewList(this.restaurantId).subscribe((data) => {
      this.reviewList = data.map((e) => {
        this.reviewId[counter++] = e.payload.doc.id;
        return e.payload.doc.data() as ReviewModel;
      });
      this.reviewList = this.reviewService.mapReviewDataWithId(this.reviewList, this.reviewId);
    });
  }

  onSelectReview(reviewId: string) {
    this.router.navigate(['review', reviewId], {
      relativeTo: this.activatedRoute
    });
  }
}
