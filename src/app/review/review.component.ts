import { Component, OnInit } from '@angular/core';
import { ReviewService } from '../services/review.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ReviewModel } from '../model/review.model';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {
  review: ReviewModel;
  reviewId: string;
  restaurantId: string;
  constructor(
    private reviewService: ReviewService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    const reviewId = this.activatedRoute.snapshot.paramMap.get('reviewId');
    this.reviewId = reviewId;
    const restaurantId = this.activatedRoute.snapshot.paramMap.get('restaurantId');
    this.restaurantId = restaurantId;
    this.reviewService.getReviewInfo(this.restaurantId, this.reviewId).subscribe((data) => {
      this.review = data as ReviewModel;
    });
  }
}
