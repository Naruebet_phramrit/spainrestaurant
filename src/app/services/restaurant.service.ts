import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { RestaurantModel } from '../model/restaurant.model';
@Injectable({
  providedIn: 'root'
})
export class RestaurantService {
  restaurantMockList = [
    { id: 1, name: 'PolaPola' },
    { id: 2, name: 'Oishi' },
    { id: 3, name: 'MaxBeef' }
  ];
  restaurantList: RestaurantModel[];
  restaurantId: string;
  restaurantRef = this.db.collection('Restaurant');
  constructor(public db: AngularFirestore) {}

  getMockRestaurantList() {
    return this.restaurantMockList;
  }

  getMockRestaurantInfo(id: number) {
    return this.restaurantMockList[id - 1];
  }

  getRestaurantList() {
    return this.restaurantRef.snapshotChanges();
  }

  getRestaurantInfo(id: string) {
    console.log(id);
    return this.restaurantRef.doc(id).valueChanges();
  }

  setCurrentRestaurant(id: string) {
    this.restaurantId = id;
  }

  mapRestaurantDataWithId(restaurantObject: RestaurantModel[], reviewId: string[]) {
    let counter = 0;
    for (counter; counter < restaurantObject.length; counter++) {
      restaurantObject[counter].id = reviewId[counter];
      console.log('inFor' + restaurantObject);
    }
    return restaurantObject;
  }
}
