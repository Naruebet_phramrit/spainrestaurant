import { Injectable } from '@angular/core';
import {
  AngularFirestore,
} from '@angular/fire/firestore';
import { RestaurantService } from './restaurant.service';
import { ReviewModel } from '../model/review.model';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {
  reviewList = [
    {
      id: 1,
      restaurantName: 'PolaPola',
      reviewText:
        'This is goodddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd',
      author: 'Aee'
    },
    {
      id: 2,
      restaurantName: 'PolaPola',
      reviewText:
        'This is moderate',
      author: 'Bee'
    },
    {
      id: 3,
      restaurantName: 'PolaPola',
      reviewText:
        'This is baddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd',
      author: 'Cee'
    }
  ];

  reviewId: string;
  restaurantRef = this.db.collection('Restaurant');

  constructor(public db: AngularFirestore, private restaurantService: RestaurantService) {}

  getMockReviewList() {
    return this.reviewList;
  }

  getMockReviewInfo(id: number) {
    return this.reviewList[id - 1];
  }

  getReviewList(restaurantId: string) {
    return this.restaurantRef.doc(restaurantId).collection('Review').snapshotChanges();
  }

  getReviewInfo(restaurantId: string, reviewId: string) {
    return this.restaurantRef.doc(restaurantId).collection('Review').doc(reviewId).valueChanges();
  }
  
  setCurrentReview(reviewId: string) {
    this.reviewId = reviewId;
  }

  mapReviewDataWithId(reviewObject: ReviewModel[], reviewId: string[]) {
    let counter = 0;
    for (counter; counter < reviewObject.length; counter++) {
      reviewObject[counter].id = reviewId[counter];
    }
    return reviewObject;
  }
}
