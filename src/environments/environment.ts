// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyA7N93XnyYbm8_4Uec8Bw9zKDji2ZCWYCo',
    authDomain: 'wongnai-demo-app.firebaseapp.com',
    databaseURL: 'https://wongnai-demo-app.firebaseio.com',
    projectId: 'wongnai-demo-app',
    storageBucket: 'wongnai-demo-app.appspot.com',
    messagingSenderId: '164368688190',
    appId: '1:164368688190:web:26f5ac50eb1d774e449d6f',
    measurementId: 'G-BJWMZJP9PV'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
